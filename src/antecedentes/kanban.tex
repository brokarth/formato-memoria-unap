\section{Team Kanban}

Team Kanban es un método que ayuda a los equipos a facilitar el flujo de valor visualizando el flujo de trabajo, estableciendo límites de Trabajo en proceso (\textit{WIP}), midiendo el rendimiento y mejorando continuamente su proceso. Los equipos ágiles tienen una opción de métodos Ágiles. La mayoría usa Scrum, un método ligero y popular para administrar el trabajo. Los equipos que desarrollan código nuevo también aplican prácticas de Programación Extrema (\textit{XP}) para enfocar la ingeniería de software y la calidad del código. Sin embargo, algunos equipos -particularmente equipos de sistemas, operaciones y equipos de mantenimiento- eligen aplicar Kanban como método principal. En estos contextos, la naturaleza rápida del trabajo, las prioridades que cambian rápidamente y el menor valor de las actividades de planificación para la próxima iteración los llevan a esta elección.

En general, Kanban se describe como un sistema de extracción. Los equipos extraen trabajo cuando saben que tienen la capacidad para hacerlo, en lugar de tener el alcance en ellos. La metodología del equipo Kanban, es un método que ayuda a facilitar el flujo de valor al visualizar el flujo de trabajo y los límites de \textit{WIP}, medir el rendimiento y mejorar continuamente el proceso.

Un sistema Kanban se compone de estados de flujo de trabajo. La mayoría de los estados tienen límites de WIP, que un elemento de trabajo puede extraerse sólo cuando el número de elementos en ese estado es inferior al límite de WIP. Algunos estados (típicamente estados inicial y final) pueden no estar limitados. Los límites de WIP son definidos y ajustados por el equipo, lo que le permite adaptarse rápidamente a las variaciones en el flujo de desarrollo del sistema complejo. Kanban se aplica junto con los requisitos de cadencia y sincronización del Agile Release Train (ART). Esto facilita la alineación, la gestión de dependencias y los ciclos de aprendizaje rápidos basados en la integración. Estos proporcionan la evidencia objetiva necesaria para avanzar en la solución más amplia.

\subsection{Descripción}

Kanban, que significa ``señal visual'', es un método para visualizar y gestionar el trabajo. Si bien hay muchas interpretaciones sobre cómo aplicar Kanban en el desarrollo, la mayoría estaría de acuerdo en que los aspectos principales incluyen lo siguiente:

\begin{itemize}
  \item El sistema contiene una serie de estados que definen el flujo de trabajo.
  \item El progreso de los elementos se rastrea visualizando todo el trabajo.
  \item Los equipos acuerdan límites de WIP específicos para cada estado y los cambian cuando es necesario para mejorar el flujo.
  \item Se adoptan políticas para especificar la gestión del trabajo.
  \item El flujo es medido. Los elementos de trabajo se rastrean desde el momento en que ingresan al sistema hasta el momento en que se van, proporcionando indicadores continuos de la cantidad de Trabajo en proceso y el tiempo de entrega actual. En otras palabras, cuánto tiempo, en promedio, se necesita un elemento para pasar por el sistema.
  \item Las clases de servicio se utilizan para priorizar el trabajo en función del costo de demora (CoD)
\end{itemize}

\subsection{Visualizando flujo y limitando WIP}
Para comenzar, los equipos suelen construir una aproximación de su flujo de proceso actual y definir algunos límites de WIP iniciales. La figura \ref{fig:kanban-board} muestra un ejemplo de la pizarra Kanban inicial de un equipo, que captura sus estados actuales de flujo de trabajo: analizar, revisar, construir e integrar y probar.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{../img/kanban-board.png}
  \caption{El tablero Kanban inicial de un equipo}
  \label{fig:kanban-board}
\end{figure}

En la figura \ref{fig:kanban-board}, el equipo también decidió crear dos almacenamientos intermedios 
(ver `Listo', arriba) para administrar mejor la variabilidad del flujo. Uno está en frente del estado de 
``revisión'', que podría requerir expertos externos en la materia (Gestión de productos u otros), cuya disponibilidad puede ser limitada y desigual. El otro buffer se encuentra frente al estado 
``integrar y probar'', que, en su caso, requiere el uso de dispositivos y recursos de prueba compartidos. Como la integración y las pruebas son realizadas por las mismas personas en la misma infraestructura, los dos estados se tratan como un solo estado.  Además, para justificar el costo de la transacción, el equipo permite límites de WIP razonablemente más altos para los estados de revisión e integración y prueba.

El tablero Kanban de un equipo evoluciona iterativamente. Después de definir el proceso inicial y los límites de WIP y ejecutar por un tiempo, los cuellos de botella del equipo deberían aparecer. De lo contrario, el equipo perfecciona el proceso o reduce aún más algunos límites de WIP hasta que se hace evidente que un estado de flujo de trabajo está ``muriendo de hambre'' o está ``demasiado lleno''. Esto ayuda al equipo a ajustar continuamente el proceso para optimizar su flujo. Por ejemplo, cambiando los límites de WIP y fusionando, dividiendo o redefiniendo `estados de flujo de trabajo.

\subsection{Midiendo el Flujo}

Los equipos de Kanban usan medidas objetivas, que incluyen el tiempo de entrega promedio, el WIP y el rendimiento para comprender y mejorar su flujo y proceso. El Diagrama de Flujo Acumulativo (CFD), ilustrado en la figura \ref{fig:cfd}, es un gráfico de área que representa la cantidad de trabajo en un estado dado, que muestra las llegadas, el tiempo en un estado, la cantidad en un estado y la salida.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.6]{../img/cfd.png}
  \caption{El CFD muestra cómo el tiempo de entrega y la WIP evolucionan con el tiempo}
  \label{fig:cfd}
\end{figure}

Cada elemento de trabajo tiene una fecha impresa, tanto cuando ingresa al Kanban 
(extraído de la lista de espera del equipo para comenzar la implementación) como cuando se completa. La curva de llegada muestra la velocidad a la que los elementos del registro atrasado entran en funcionamiento. La curva de salida muestra cuando han sido aceptados. El eje x muestra el tiempo de entrega promedio: cuánto tarda, en promedio, un elemento en pasar por el sistema. El eje y muestra el WIP, la cantidad promedio de elementos en el sistema en cualquier punto en el tiempo.

El rendimiento (la cantidad de Historias completadas por un período de tiempo) representa otra medida crítica. Como los equipos de Kanban operan con una cadencia de iteración, miden el rendimiento en varias historias por iteración.

El CFD proporciona los datos para que el equipo pueda calcular su rendimiento de iteración actual. Para llegar al número promedio de historias procesadas por día, el equipo divide el WIP promedio por el tiempo de entrega promedio. Luego lo multiplican por 14, que es el número de días en una iteración de dos semanas. Esto proporciona el rendimiento promedio de historias por iteración, lo que ayuda con la planificación.

El CFD también proporciona una visualización importante de las variaciones de flujo significativas, que pueden ser el resultado de impedimentos internos sistémicos que el equipo no conoce o fuerzas externas que impiden el flujo. El CFD es un excelente ejemplo de una medida objetiva que facilita una mejora incesante para los equipos de Kanban.

\subsection{Mejorando el flujo con clases de servicio}
Además, los equipos deben ser capaces de administrar las dependencias y asegurar la alineación con los hitos. Kanban usa el concepto de clases de servicio para ayudar a los equipos a optimizar la ejecución de sus elementos atrasados. Las clases de servicio ayudan a diferenciar elementos atrasados en función de su CoD. Cada clase de servicio tiene una política de ejecución específica que el equipo acepta seguir. Por ejemplo:

\begin{itemize}
  \item \textbf{Estándar}: Representa la clase de servicio de referencia, aplicable a los elementos de trabajo que no tienen una fecha acelerada o fija. La mayoría de los ítems retrasados deberían incluirse en esta categoría. El CoD es lineal para los ítems estándar, lo que significa que el valor no se puede alcanzar hasta que se produzca la entrega, pero no hay un requisito de fecha fija.
  \item \textbf{Fecha fija}: Describe los elementos de trabajo que se deben entregar en o antes de una fecha específica. Típicamente, el CoD de estos ítems no es lineal y es muy sensible a pequeños cambios en la fecha de entrega; estos deben ser gestionados activamente para mitigar el riesgo del cronograma. Por lo tanto, estos elementos se incorporan al desarrollo cuando es necesario finalizarlos a tiempo. Algunos elementos pueden requerir un análisis adicional para refinar el tiempo de espera esperado. Algunos deben ser reclasificados como expeditos si el equipo se retrasa.
  \item \textbf{Expedito}: Un ítem de retraso acumulado tiene un CoD inaceptable y por lo tanto requiere atención inmediata. Se puede incorporar al desarrollo, incluso en violación de los límites de WIP actuales. Por lo general, solo puede haber un ítem expedito en el sistema a la vez, y los equipos pueden establecer una política para enfocarse en ese ítem para asegurarse de que se mueva a través del sistema rápidamente.
\end{itemize}

Si los equipos encuentran que muchos elementos requieren agilización, entonces el sistema puede estar sobrecargado. O la demanda excede la capacidad, o el proceso de entrada puede necesitar más disciplina. En cualquier caso, el proceso debe ajustarse. Como se ilustra en la figura \ref{fig:classes-of-services}, las clases de servicio se visualizan típicamente como ``carriles de natación''.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{../img/classes-of-services.png}
  \caption{Clases de servicio en el tablero de Kanban}
  \label{fig:classes-of-services}
\end{figure}

Además, los equipos pueden usar colores específicos para diferentes tipos de elementos atrasados, como nueva funcionalidad, investigación de \textit{Spike} y modelado. Esto agrega claridad al trabajo que se está realizando.

Dar atención al proceso de flujo permite a los equipos de Kanban oportunidades de mejora que de otro modo pasarían desapercibidas. Por ejemplo, los cambios en el CFD pueden sugerir un aumento del WIP promedio 
(lo que provocará un aumento en el tiempo de entrega). Si bien esto puede ser solo un síntoma de un problema más profundo, el equipo ahora tiene una forma de detectarlo. La reflexión regular y la adaptación del proceso son necesarias para obtener el beneficio de una alta visibilidad del flujo.

\subsection{El equipo Kanban se une}
Los equipos de Kanban operan en un contexto más amplio, construyendo una solución que requiere múltiples equipos ágiles y / o la colaboración. Para lograr esto, el equipo debe adherirse a las reglas y pautas regulares de Kanban. Las reglas son que los equipos planifiquen juntos, se integren y hagan demostraciones juntos, y aprendan juntos. La planificación conjunta es un elemento que merece mayor
 discusión, como se describe en las siguientes secciones.

\subsection{Estimando trabajo}
Los equipos de Kanban no invierten tanto tiempo en estimar como lo hacen los equipos de Scrum. En su lugar, analizan el trabajo necesario, dividen los ítems más grandes cuando es necesario y completan las historias resultantes, en su mayoría sin preocuparse por su tamaño. Sin embargo, los equipos deben ser capaces de estimar la demanda en relación con su capacidad para la planificación \textit{PI}, y también ayudan a estimar los ítems atrasados más grandes. Por otra parte, la previsión requiere una comprensión de la velocidad del equipo de una manera que sea coherente con los otros equipos.

\subsection{Establecer un punto de partida común para la estimación}
Inicialmente, un nuevo equipo de Kanban no conoce su rendimiento, ya que es una medida final basada en la historia. Para comenzar, necesitan una forma de estimar el trabajo, a menudo comenzando con la primera sesión de planificación \textit{PI}. De una manera consistente con los equipos de Scrum, la estimación de la capacidad inicial comienza con la estimación normalizada. Los equipos de Kanban luego suman sus historias estimadas en iteraciones, tal como lo hacen los equipos de Scrum. Su capacidad de inicio es su velocidad supuesta, al menos para el primer \textit{PI}.

\subsection{Cálculo de velocidad derivada}

Después de este punto de partida, los equipos de Kanban pueden usar su CFD para calcular su rendimiento real en historias por iteración. O simplemente pueden contarlos y promediarlos. Los equipos Kanban, luego, calculan su velocidad derivada multiplicando el rendimiento por un tamaño de historia promedio 
(típicamente de tres a cinco puntos). De esta forma, los equipos ScrumXP y Kanban pueden participar en el marco económico más grande, que a su vez proporciona el contexto económico principal para la cartera.

\subsection{Estimación de ítems de trabajo más grandes}
En el portafolio y en los grandes niveles de solución, a menudo es necesario estimar los ítems de trabajo más grandes para determinar su viabilidad económica potencial (Épicas y Capacidades). Además, desarrollar un programa Roadmap requiere:

\begin{itemize}
  \item Un conocimiento de la estimación (qué tan grande es el ítem)
  \item Velocidad de \textit{ART} (cuánta capacidad tiene el \textit{ART} para hacerlo)
\end{itemize}

Los artículos más grandes tienden a ser más difíciles de estimar, lo que aumenta el riesgo. Por lo tanto, se dividen en historias de usuarios, al igual que los equipos de ScrumXP, para mejorar la comprensión, aumentar la precisión de la estimación y facilitar que el \textit{PO} 
priorice el trabajo. Las historias se estiman en puntos de historia normalizados. Esto proporciona la posibilidad de que la Empresa combine estimaciones de varios tipos de equipos, sin debate excesivo.