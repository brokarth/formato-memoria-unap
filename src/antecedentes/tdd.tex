\section{Pruebas primero}
TDD (Test driven development o desarrollo dirigido por pruebas) es una práctica de calidad que viene de la programación extrema, que recomienda escribir pruebas antes de escribir el código productivo mejorando la entrega enfocándose en los resultados esperados.

Las pruebas ágiles difieren del big-bang, enfoque de pruebas tradicional, que prueba todo al final del desarrollo. En su lugar, el código es desarrollado y testeado en pequeños incrementos, normalmente con el desarrollo de la prueba antes de escribir el código. De esta forma, las pruebas ayudan a elaborar y definir mejor el comportamiento esperado del sistema incluso antes de que el sistema sea codificado. La calidad es construido desde el comienzo. Este enfoque de “justo a tiempo” a la construcción del comportamiento propuesta del sistema también mitiga la necesidad de especificaciones de requisitos muy detalladas y aprobaciones que son normalmente en el desarrollo tradicional de software para controlar la calidad. Incluso mejor, estas pruebas,  a diferencia de los requerimientos normalmente escritos, están automatizados siempre que es posible. Y cuando no lo son, siguen dando información sobre qué debería hacer el sistema, más que una declaración de ideas tempranas sobre lo que se supone debería hacer.

\subsection{Detalles}
Prueba ágiles es un proceso continuo que es integral para \textit{Lean} y la calidad de código. En otras palabras, los equipos ágiles no pueden avanzar rápidamente sin asegurar la calidad del software y eso no lo pueden lograr sin pruebas automatizadas y, donde sea posible, desarrollo dirigido por pruebas.

\subsection{Matriz de pruebas ágiles}
El defensor de programación extrema y coautor del manifiesto ágil, Brian Marick, ayudó a los pioneros en pruebas ágiles creando una matriz que guía el razonamiento detrás de los test. Este aproximamiento fue desarrollado más allá en pruebas ágiles y extendido por el paradigma agil \textit{Agile software requirements} \cite{agile-testing, agile-software-requirements}

La figura \ref{fig:agile-test} describe y extiende la matriz original ayudando a guiar en qué y cuándo se debe realizar un test.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.7]{../img/agile-testing.png}
  \caption{Matriz de pruebas ágiles}
  \label{fig:agile-test}
\end{figure}

El eje horizontal de la matriz contiene pruebas de negocio o tecnología. Las pruebas de negocio son entendidas por el usuario y escritas con terminología de negocio. Las pruebas de tecnología están escritas en el lenguaje de desarrollo y son usadas para evaluar si el código entrega el comportamiento para el cual fue desarrollado.

El eje vertical contiene pruebas apoyando el desarrollo (evaluando el código interno) o criticando la solución (evaluando el sistema contra los requerimientos de usuarios)

La clasificación dentro de los cuatro cuadrantes (Q1 - Q4) permite una estrategia de pruebas comprensible que ayuda a asegurar la calidad.

\begin{itemize}
\item \textbf{Q1} - Contiene pruebas unitarias y de componentes. Las pruebas son escribas para correr antes y después que el código cambia confirmando que el sistema funciona como es esperado.
\item \textbf{Q2} - Contiene los test funcionales (pruebas de aceptación del usuario), para las historias, funcionalidades y capacidades para validar que funcionan de la forma que el dueño del producto espera. Pruebas de aceptación de funcionalidades y capacidades confirman el comportamiento en conjunto de muchas historias de usuario. Los equipos automatizan estos test siempre que sea posible y usan pruebas manuales solo cuando no hay otra alternativa
\item \textbf{Q3} - Contiene pruebas de aceptación a nivel de sistema para validar que el comportamiento de todo el sistema llegan a los requerimientos de usabilidad y funcionalidad esperada, incluidos los escenarios que a menudo se encuentran en el uso real del sistema. Estos pueden incluir pruebas exploratorias, prueba de aceptación del usuario, pruebas basadas en escenarios y pruebas finales de usabilidad. Debido a que involucran a usuarios y testers involucrados en escenarios de implementación reales o simulados, estas pruebas son a menudo manuales. Con frecuencia son la validación final del sistema antes de la entrega del sistema al usuario final.
\item \textbf{Q4} - Contiene pruebas de calidad del sistema para verificar que el sistema cumpla con sus requisitos no funcionales. Por lo general, son compatibles con un conjunto de herramientas de prueba automatizadas, como carga y rendimiento, diseñadas específicamente para este propósito. Como cualquier cambio en el sistema pueden pasar a llevar los requisitos no funcionales, deben ejecutarse de forma continua, o al menos siempre que sea práctico
\end{itemize}

Los cuadrantes uno y dos definen la funcionalidad del sistema. Las prácticas de prueba primero incluyen desarrollo dirigido por pruebas (TDD) y desarrollo dirigido por pruebas de aceptación (ATDD). Ambos implican crear la prueba antes de desarrollar el código y usar la automatización de prueba para apoyar la integración continua, la velocidad del equipo y la efectividad del desarrollo.

\subsection{Desarrollo dirigido por pruebas (TDD)}
Beck y otras personas han definido una serie de prácticas de XP que las han llamado TDD
\cite{test-driven-development}:

\begin{itemize}
\item Escribe la prueba primero, que asegura que el desarrollador entiende el comportamiento esperado antes.
\item Corre los test y mira que falle. Porque no hay código aun, esto puede parecer tonto al comienzo, pero sirve para dos buenas razones: verifica que el test funciona, incluyendo su soporte (librerías, compiladores, etc), y demuestra como el test se comportara si el código falla.
\item Escribe la cantidad mínima de código para que el test pase. Si falla, reescribe el código hasta que pase, pero siempre con el código mínimo necesario.
\end{itemize}

En \textit{XP}, esta práctica se diseñó principalmente para operar en el contexto de pruebas unitarias, que son pruebas escritas por el desarrollador (también código) que evalúan las clases y métodos utilizados. Estas son una forma de ``prueba de caja blanca'' porque prueban las partes internas del sistema y las diversas rutas de código. El trabajo en parejas es cuando dos personas colaboran simultáneamente para desarrollar el código y las pruebas, proporcionando una revisión por pares integrada, lo que ayuda a asegurar una alta calidad. Incluso cuando no se hace trabajo en pareja, las pruebas dan otro conjunto de ojos que revisan el código. Los desarrolladores a menudo refactorizan el código para pasar la prueba de la manera más simple y elegante posible, que es una de las razones principales por las que se confía en \textit{TDD}.

\subsection{Pruebas unitarias}

Casi siempre \textit{TDD} involucra pruebas unitarias, lo que previene que \textit{QA} y el personal de prueba pasen la mayor parte del tiempo buscando e informando errores a nivel de código, lo que permite un enfoque adicional en los desafíos de prueba a nivel del sistema, donde se identifican comportamientos más complejos basados en las interacciones entre los módulos del sistema. La comunidad de código abierto ha construido \textit{frameworks} de pruebas unitarias para cubrir la mayoría de los lenguajes, incluidos Java, C, C\#, C++, XML, HTTP y Python. Ahora existen más \textit{frameworks} de pruebas unitarias para la mayoría de construcciones de código que un desarrollador encontrará. Proporcionan el soporte para el desarrollo y mantenimiento de pruebas unitarias y para ejecutarlas automáticamente contra el sistema.

Como las pruebas unitarias se escriben antes o al mismo tiempo que el código, y sus \textit{frameworks} incluyen la automatización de la ejecución de pruebas, las pruebas unitarias pueden realizarse dentro de la misma iteración. Además, los \textit{frameworks} de prueba unitarias mantienen y administran las pruebas unitarias acumuladas. Como resultado, la automatización de pruebas de regresión para pruebas unitarias es mayormente gratuita para el equipo. Las pruebas unitarias son la piedra angular del software ágil, y cualquier inversión realizada en pruebas unitarias comprensibles generalmente mejora la calidad y la productividad.

\subsection{Pruebas de componentes}

De forma similar, los equipos usan pruebas para evaluar componentes del sistema a mayor escala. Muchos de estos están presentes en varias capas de arquitectura, donde proporcionan servicios necesitados por funcionalidades de la aplicación o módulos externos. Las herramientas y prácticas para implementar pruebas de componentes varían. Por ejemplo, los \textit{frameworks} de pruebas pueden contener pruebas unitarias complicadas escritas en el lenguaje del \textit{frameworks}
(por ejemplo, Java, C, C\#, etc.). Como resultado, muchos equipos usan sus \textit{frameworks} de pruebas unitarias para construir pruebas de componentes. Puede que ni siquiera los consideren como funciones separadas, ya que es simplemente parte de su estrategia de prueba. En otros casos, los desarrolladores pueden incorporar otras herramientas de prueba o escribir pruebas completamente personalizadas en cualquier lenguaje o entorno que sea más productivo para que prueben comportamientos más amplios del sistema. Estas pruebas también son automáticas, y sirven como una defensa primaria contra las consecuencias imprevistas de la refactorización y el nuevo código.

\subsection{Desarrollo dirigido por pruebas de aceptación (ATDD)}

El cuadrante dos de la Matriz Ágil de Pruebas muestra que primero se aplica la prueba también a pruebas de historias, características y capacidades, como lo hace a pruebas unitarias, lo cual se llama Desarrollo Impulsado por Pruebas de Aceptación (\textit{ATDD}). Y ya sea que se adopte de manera formal o informal, a muchos equipos les resulta más eficiente escribir primero la prueba de aceptación antes de desarrollar el código. Después de todo, el objetivo es hacer que todo el sistema funcione según lo previsto. Ken Pugh señala que el énfasis está más en expresar los requisitos en términos inequívocos que en centrarse en la prueba per se \cite{lean-agile-acceptance}. Además, observa que hay tres etiquetas alternativas para este proceso de detallado. \textit{ATDD}, Especificación por ejemplo (\textit{SBE}) y Diseño impulsado por comportamiento (\textit{BDD}). Hay algunas pequeñas diferencias en estos enfoques, pero todos enfatizan los requisitos de comprensión antes de la implementación. En particular, \textit{SBE} sugiere que el \textit{PO} debería proporcionar ejemplos realistas en lugar de declaraciones abstractas, ya que a menudo no escriben las pruebas de aceptación en sí mismas.

\subsection{Pruebas funcionales}
Las pruebas de aceptación de historias confirman que cada nueva historia de usuario implementada entrega su comportamiento previsto durante la iteración. Si estas historias funcionan según lo previsto, entonces es probable que cada mejora de software finalmente satisfaga las necesidades de los usuarios.

Durante un Incremento de programa (\textit{PI}), se realizan pruebas de aceptación de funcionalidades y capacidades, usando pruebas similares. La diferencia es que las pruebas de capacidad operan en el siguiente nivel de abstracción, por lo general muestran cómo varias historias trabajan juntas para entregar una cantidad de valor más significativa para el usuario. Por supuesto, puede haber fácilmente múltiples pruebas de aceptación de funcionalidades asociadas con una funcionalidad más compleja. Y lo mismo ocurre con las historias, verificando que el sistema funciona según lo previsto para todos los niveles de abstracción.

Las siguientes son características de las pruebas funcionales:

\begin{itemize}
  \item Escrito en el idioma del negocio
  \item Desarrollado en una conversación entre desarrolladores, testers y el \textit{PO}
  \item ``Pruebas de caja negra'' para verificar que solo las salidas del sistema cumplen con sus condiciones de satisfacción, sin preocuparse por el funcionamiento interno del sistema
  \item Ejecutar en la misma iteración que el desarrollo del código
\end{itemize}

Aunque todo el mundo puede escribir pruebas, tanto el \textit{PO} como propietario del negocio (\textit{BO}) es responsable de la eficacia de las pruebas. Si una historia no pasa su prueba, los equipos no obtienen ningún crédito por esa historia, y se transfiere a la siguiente iteración para corregir la prueba o el código.

Las funcionalidades, capacidades e historias deben pasar una o más pruebas de aceptación para cumplir con su definición de “hecho”. Y puede haber múltiples pruebas asociadas con un elemento de trabajo particular.

\subsection{Automatizando la Prueba de Aceptación}
Debido a que las pruebas de aceptación se ejecutan en un nivel superior al código, hay una variedad de enfoques para ejecutarlas, incluido su manejo como pruebas manuales. Sin embargo, las pruebas manuales se acumulan muy rápido. Cuanto más rápido vayas, cuanto más rápido crecen, más lento vas. Eventualmente, la cantidad de trabajo manual requerido para ejecutar las pruebas de regresión ralentiza el equipo y causa retrasos en la entrega de valor.
Los equipos saben que para evitar esto, deben automatizar la mayoría de sus pruebas de aceptación mediante el uso de una variedad de herramientas, que incluyen el lenguaje de programación de destino (por ejemplo, Perl, PHP, Python, Java) o lenguaje natural según lo soportan los \textit{frameworks} de prueba específicos, como \textit{Cucumber}. O tal vez utilicen formatos de tabla como el ``\textit{Framework} para pruebas integradas'' (\textit{FIT}). El enfoque preferido es usar un nivel más alto de abstracción que trabaje en contra de la lógica comercial de la aplicación, lo que impide que la capa de presentación u otros detalles de implementación bloqueen las pruebas.
