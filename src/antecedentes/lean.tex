\section{Lean}
La forma de pensar de Lean-Agile es la combinación de creencias, asunciones y las acciones de los líderes y practicantes que adoptan los conceptos del manifiesto Ágil y el pensamiento Lean. Es la base personal, intelectual y de liderazgo para adoptar y aplicar los principios y prácticas.

\textit{Lean} está basado en tres principios básicos: Desarrollo Ágil, pensamiento sistemático y desarrollo de productos Lean \cite{safe}.

\textit{Agile} da las herramientas para empoderar y motivar a los equipos a lograr altos niveles de productividad, calidad y compromiso. Pero se requieren vastos y profundos conocimientos y prácticas en \textit{Lean-Agile} para poder lograr hacer desarrollos \textit{Lean} y \textit{Agile} 
completamente en compañías grandes. Por lo tanto, hay dos aspectos primarios en la forma de trabajar 
\textit{Lean-Agile}.

\begin{itemize}
  \item Pensamiento Lean: El pensamiento \textit{Lean} esta ilustrado por la casa segura de Lean 
  (The SAFe house of Lean) \textit{(figura \ref{fig:lean-agile})}. El techo representa el objetivo de la entrega de valor. Los pilares representan el respeto por las personas y la cultura, flujo, innovación y mejora continua que apoyan el objetivo. El liderazgo \textit{Lean} provee el suelo donde todo se sostiene.
  \item Adoptando Agilismo: Construido completamente en las habilidades, aptitudes y capacidades de los equipos Ágiles y sus líderes. El manifiesto Ágil provee un sistema de valores y principios que son fundamentales para trabajar desarrollos exitosos Ágiles.
\end{itemize}

Entendiendo y aplicando estos conocimientos ayuda a crear el pensamiento \textit{Lean-Agile}, parte de la nueva forma de hacer gestión y mejora de la cultura de compañía. Esto entrega el liderazgo necesario para dirigir una transformación exitosa a Lean. Ayudando a negocios e individuos lograr sus objetivos.

\subsection{La casa segura de Lean (The SAFe house of Lean)}
Inicialmente derivada de la manufacturación Lean, los principios y prácticas del pensamiento Lean aplicados al software, productos y desarrollo de sistemas ahora son vastos y profundos
\cite{lean-production-toyota}. Por ejemplo Ward \cite{lean-product}, Reinertsen \cite{development-flow}, Poppendieck \cite{concept-to-cash}, Leffingwell \cite{agile-software-requirements}, y otros han descrito aspectos del pensamiento \textit{Lean}, poniendo mucho de los principales principios y prácticas en el contexto de desarrollo de producto. Junto con esto, se presenta \textit{the SAFe house of Lean} 
como está ilustrado en la figura \ref{fig:lean-agile}, inspirado en la casa de Lean de Toyota y otros.

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.5]{../img/lean-agile.png}
  \caption{The SAFe house of Lean}
  \label{fig:lean-agile}
\end{figure}

\subsection{El objetivo - Valor}
El objetivo de Lean es entregar el máximo valor al cliente, en el más corto y sustentable tiempo, mientras se entrega la más alta calidad posible al cliente y sociedad como un todo. Alta moral, seguridad y deleite del cliente son objetivos y beneficios adicionales.

\subsection{Pilar 1 - Respeto por la gente y la cultura}
Un enfoque \textit{Lean-Agile} no implementa ni genera ningún trabajo por sí solo. Las personas lo hacen. Respeto por la gente y la cultura es una necesidad básica humana. Las personas están empoderadas en evolucionar sus prácticas y mejoras. Las gerencias desafían a los equipos a cambiar para dirigirlos a un mejores formas de trabajo. Sin embargo, son los equipos e individuos quienes aprenden resolución de problemas y habilidades de reflexión, y son responsable de hacer las mejoras apropiadas.

La fuerza conductora detrás de este nuevo comportamiento es la cultura, la cual requiere que las compañías y sus líderes cambien primero. El principio del respecto por la gente y la cultura se debe extender a las relaciones con proveedores, socios, clientes y la comunidad que apoya a la compañía.

\subsection{Pilar 2 - Flujo}
La clave para una ejecución exitosa de Lean es establecer un continuo flujo de trabajo que soporta la entrega de valor incremental, basado en las constante de feedback y cambio. Flujo constante disponibiliza rápida entrega de valor, prácticas que garantizan calidad efectiva, rápidas mejoras y dirección basado en evidencia.

Los principios del flujo son una parte esencial de \textit{Lean-Agile}. Esto incluye entender la cadena completa de valor, visualizando y limitando el trabajo en progreso, y reduciendo los trabajos por lotes y gestionando el tamaño de las colas. Adicionalmente, \textit{Lean} se centra en enfocar y eliminar continuamente cualquier desperdicio de tiempo (actividades que no aportan valor).

\textit{Lean-Agile} provee un mejor entendimiento del proceso de desarrollo de sistemas, incorporando nuevos pensamientos, herramientas y técnicas que líderes y equipos pueden usar para lograr pasar a \textit{DevOps} y despliegue continuo que extienden el flujo a todo el proceso de entrega de valor.

\subsection{Pilar 3 - Innovación}

El flujo construye una sólida función para la entrega de valor. Pero sin innovación, tanto el producto como el proceso decaerían de forma constante. Para apoyar esta crítica parte de Lean, los líderes de Lean-Agile: 

\begin{itemize}
  \item Salen de sus oficinas para ir a los lugares de trabajo donde el valor es realmente producido y el producto es creado y utilizado. Como Taiichi Ohno dijo, “Ninguna mejora importante fue hecho detrás de un escritorio”
  \item Entregan tiempo y espacio para que las personas sean creativas, permitiendo innovación intencional, el cual raramente puede ocurrir en la presencia de utilización del 100\% y ``combates de incendios'' diarios. Iteraciones de innovación y planificación de Lean son una buena oportunidad
  \item Aplicar exploración continua, el proceso de constantemente buscar el mercado y las necesidades de usuario y definir una visión, camino y un conjunto de funcionalidades que satisfagan dichas necesidades
  \item Aplicar contabilidad de exploración \cite{lean-startup}. Estableciendo métricas que no sean ni de finanzas ni banales que proveen rápido feed sobre la innovación
  \item Validando la innovación con los clientes. Entonces cambiar sin miedo ni culpa cuando una hipótesis necesita cambiar.
\end{itemize}

\subsection{Pilar 4 - Mejora constante}
El cuarto pilar, mejora constante, alienta a aprender y crecer a traves de reflexión continua y mejoras de procesos. Un constante sentimiento de peligro competitivo hace que la compañía persiga oportunidades de mejora agresivamente. Líderes y equipos hacen lo siguiente.

\begin{itemize}
  \item Optimizar como un todo, no partes pequeñas, tanto de la organización como de los procesos de desarrollo.
  Considerar hechos cuidadosamente y rápidamente actuar.
  \item Aplicar técnicas y herramientas \textit{Lean} para descubrir la raíz que causa ineficiencias y aplicar contramedidas rápidamente.
  \item Reflejar los hitos importantes para identificar abiertamente y señalar deficiencias de los procesos en todos los niveles.
\end{itemize}

\subsection{Soporte - Liderazgo}
El soporte de Lean es el Liderazgo, el habilitar clave para llegar al éxito. La responsabilidad máxima a la hora de adoptar un enfoque \textit{Lean-Agile} recae en las gerencias de las compañías, líderes y ejecutivos. De acuerdo a Demming, “Tal responsabilidad no puede ser delegada a encargados de Lean-Agile, grupos de trabajo, la gerencia de programas de la compañía, equipos de proceso, consultores externos o cualquier otro grupo“ \cite{out-of-the-crisis}. Los líderes deben ser entrenado en esta nuevas e innovadoras formas de pensar y exhibir los principios y el comportamiento de liderazgo \textit{Lean-Agile}

El pensamiento \textit{Lean} es similar, pero diferente a \textit{Agile}, inicialmente introducido como enfocado en el equipo que tiende a excluir a la gerencia. Infortunadamente, eso no escala. Mientras que en \textit{Lean-Agile} las gerencias se convierten en líderes que adoptan los valores de 
\textit{Lean}, es competente en las prácticas básicas, proactivamente elimina impedimentos y toma un rol activo en dirigir el cambio organizacional y facilitar una mejora constante.

\subsection{El manifiesto Ágil}
En 1990, respondiendo a los muchos retos del proceso de la metodología cascada, algunos métodos de desarrollo livianos e iterativos comenzaron a aparecer. En 2001 muchos de estos líderes de marcos de trabajo se juntaron en una Snowbird, Utah. Mientras que habian diferencias de opiniones en cosas muy específicas de un método sobre otro, los asistentes entendieron que sus valores compartidos y creencias eliminaban sus diferencias. El resultado fue el manifiesto ágil de desarrollo de software, un punto de inflexión que aclaró el nuevo enfoque y comenzó a llevar los beneficios que estos métodos innovadores a toda la industria del desarrollo \cite{agile-manifesto}. El manifiesto consiste de la declaración de los valores y un conjunto de principios, todo esto visto en capítulo de Agile correspondiente. Junto con varias prácticas, el manifiesto Ágil provee las herramientas para el empoderamiento y equipos autoorganizados. Lean extiende este concepto a los equipos, aplicado pensamiento \textit{Lean} para entender y mejorar continuamente los sistemas que soportan el trabajo crítico.